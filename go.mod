module gitlab.com/merfrei/ubur

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
)
