package config

import "github.com/BurntSushi/toml"

// Mongo is the MongoDB config
type Mongo struct {
	URI      string `toml:"uri"`
	Database string `toml:"database"`
}

// ProxyService contains the config for the PS API
type ProxyService struct {
	URL    string `toml:"url"`
	APIKey string `toml:"api_key"`
}

// Config is the app config
type Config struct {
	Mongo        Mongo
	ProxyService ProxyService
}

// Load receive the config and data and generate/return a new Config
func Load(data []byte) *Config {
	config := Config{}
	toml.Unmarshal(data, &config)
	return &config
}
