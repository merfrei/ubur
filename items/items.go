package items

// Item is the interface for all the collected items
type Item interface {
	// GetIdentifier must return a valid and unique identifier
	GetIdentifier() string
	// GetT returns a serializable object to use in an UPDATE operation
	GetT() interface{}
	// SetStatus write a custom status to the item
	SetStatus(status string)
}
