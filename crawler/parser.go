package crawler

import (
	"context"
	"log"
	"net/http"
	"sync"
)

type parserF func(context.Context, *http.Response) <-chan interface{}

// Parse will parse the responses body using the function parse
// return a new stream with results
func Parse(ctx context.Context, wg *sync.WaitGroup,
	parse parserF, responses <-chan *http.Response, desc string) chan interface{} {
	resultsStream := make(chan interface{})

	parseResults := func(wg *sync.WaitGroup, rs <-chan interface{}) {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case result, ok := <-rs:
				if !ok {
					return
				}
				select {
				case <-ctx.Done():
					return
				case resultsStream <- result:
				}
			}
		}
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(resultsStream)

		var pw sync.WaitGroup
		for {
			select {
			case <-ctx.Done():
				log.Printf("PARSER [%s]: canceling - reason: %v\n", desc, ctx.Err())
				goto end
			case r, ok := <-responses:
				if !ok {
					log.Printf("PARSER [%s]: done\n", desc)
					goto end
				}
				pw.Add(1)
				go parseResults(&pw, parse(ctx, r))
			}
		}
	end:
		pw.Wait()
	}()

	return resultsStream
}
