package crawler

import (
	"context"
	"log"
	"net/url"
	"sync"
)

// ResultsToURL receives a stream of results and return a new stream of URLs
func ResultsToURL(ctx context.Context, wg *sync.WaitGroup, results <-chan interface{}) <-chan *url.URL {
	urls := make(chan *url.URL)
	go func() {
		defer wg.Done()
		defer close(urls)
	loop:
		for {
			select {
			case <-ctx.Done():
				log.Printf("Results to URL canceled: %+v", ctx.Err())
				return
			case r, rok := <-results:
				if !rok {
					log.Println("Results to URL completed")
					return
				}
				url, uok := r.(*url.URL)
				if !uok {
					log.Println("Result to URL: not a valid URL: ", url)
					continue loop
				}
				select {
				case <-ctx.Done():
					log.Printf("Results to URL canceled: %+v", ctx.Err())
					return
				case urls <- url:
				}
			}
		}
	}()
	return urls
}
