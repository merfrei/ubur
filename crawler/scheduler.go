package crawler

import (
	"context"
)

// Scheduler is an interface to implement by any requests scheduler
type Scheduler interface {
	Schedule(context.Context, *Request)
	Append(context.Context, <-chan *Request)
	Requests() <-chan *Request
	Done() <-chan interface{}
}

// BasicScheduler is an implementation of the Scheduler
// for using on most of the crawlers
type BasicScheduler struct {
	name     string
	requests chan *Request
	done     chan interface{}
}

// NewBasicScheduler creates a returns a new scheduler
func NewBasicScheduler(bsize uint, name string) *BasicScheduler {
	return &BasicScheduler{
		name:     name,
		requests: make(chan *Request, bsize),
		done:     make(chan interface{}),
	}
}

// Schedule will schedule a new request
func (s *BasicScheduler) Schedule(ctx context.Context, r *Request) {
	select {
	case <-ctx.Done():
	case s.requests <- r:
	}
}

// Append reads from a stream of requests and schedule them
func (s *BasicScheduler) Append(ctx context.Context, requests <-chan *Request) {
	for {
		select {
		case <-ctx.Done():
			return
		case r, ok := <-requests:
			if !ok {
				close(s.done)
				return
			}
			select {
			case <-ctx.Done():
			case s.requests <- r:
			}
		}
	}
}

// Requests returns a read only channel for the requests
func (s *BasicScheduler) Requests() <-chan *Request {
	return s.requests
}

// Done returns a read only channel closed when it's done
func (s *BasicScheduler) Done() <-chan interface{} {
	return s.done
}
